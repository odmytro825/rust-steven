use near_sdk::borsh::{self, BorshDeserialize, BorshSerialize};
use near_sdk::{env, near_bindgen};

near_sdk::setup_alloc!();

// add the following attributes to prepare your code for serialization and invocation on the blockchain
// More built-in Rust attributes here: https://doc.rust-lang.org/reference/attributes.html#built-in-attributes-index
#[near_bindgen]
#[derive(Default, BorshDeserialize, BorshSerialize)]

pub struct Steven {
    time: u64,
    transaction_hash: String,
    operation: String,
}

impl Steven {
    // to get the current time in nanoseconds
    pub fn new(transaction_hash: String, operation: String) -> Self {
        Self {
            time: env::block_timestamp(),
            transaction_hash,
            operation,
        }
    }
    // to get given time or call function new to the current time in nanoseconds
    pub fn get_time(&self) -> u64 {
        self.time
    }
    // to get the transaction hash
    pub fn get_transaction_hash(&self) -> String {
        self.transaction_hash.clone()
    }
    // to get the operation
    pub fn get_operation(&self) -> String {
        self.operation.clone()
    }
    // to set the time
    pub fn set_time(&mut self, time: u64) {
        self.time = time;
    }
    // to set the transaction hash
    pub fn set_transaction_hash(&mut self, transaction_hash: String) {
        self.transaction_hash = transaction_hash;
    }
    // to set the operation
    pub fn set_operation(&mut self, operation: String) {
        self.operation = operation;
    }
    // to get the output in required format
    pub fn to_string(&self) -> String {
        format!(
            "Steven {{ time: {}, transaction_hash: {}, operation: {} }}",
            self.time, self.transaction_hash, self.operation
        )
    }
}
// use the attribute below for unit tests
#[cfg(not(target_arch = "wasm32"))]
#[cfg(test)]
mod tests {
    use super::*;
    use near_sdk::MockedBlockchain;
    use near_sdk::{testing_env, VMContext};
    fn get_context(predecessor_account_id: String, storage_usage: u64) -> VMContext {
        VMContext {
            current_account_id: "alice.testnet".to_string(),
            signer_account_id: "jane.testnet".to_string(),
            signer_account_pk: vec![0, 1, 2],
            predecessor_account_id,
            input: vec![],
            block_index: 0,
            block_timestamp: 0,
            account_balance: 0,
            account_locked_balance: 0,
            storage_usage,
            attached_deposit: 0,
            prepaid_gas: 10u64.pow(18),
            random_seed: vec![0, 1, 2],
            is_view: false,
            output_data_receivers: vec![],
            epoch_height: 19,
        }
    }

    // Tests

    fn output () {

    }
}
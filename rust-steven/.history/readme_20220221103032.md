# Add wasm target to blockchain

rustup target add wasm32-unknown-unknown

# Install Near-cli

npm install -g near-cli

# To run tests

cargo test -- --nocapture

# Compile

cargo build --target wasm32-unknown-unknown --release